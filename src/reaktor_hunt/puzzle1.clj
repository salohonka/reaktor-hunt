(ns reaktor-hunt.puzzle1
  (:require [clojure.string :as s]))

(def bytes (->> "/home/matias/Work/reaktor-hunt/resources/puzzle1"
                slurp s/split-lines (mapv #(partition 8 %))))

(def line-length (count (first bytes)))

(def ints (partition line-length (for [line bytes
                                       byte line]
                                   (Integer/parseInt (apply str byte) 2))))

(defn valid-byte? [n] (<= n line-length))

(defn get-password-char [position ints]
  (let [value-at-pos (get ints position)]
    (if-not (valid-byte? value-at-pos)
      (char value-at-pos)
      (recur value-at-pos ints))))

(defn find-first [pred coll]
  (some #(when (pred %) %) coll))

(comment
  (apply str (map #(get-password-char (find-first valid-byte? %)
                                      (vec %))
                  ints)))
