(ns reaktor-hunt.puzzle3
  (:require [clojure.string :as s]))

(def data (for [[coordinate steps] (->> (slurp "/home/matias/Work/reaktor-hunt/resources/puzzle3")
                                        (s/split-lines)
                                        (map #(s/split % #" ")))
                :when (seq steps)]
            {(as-> coordinate $
                   (s/split $ #",")
                   (mapv #(Integer/parseInt %) $))
             (s/split steps #",")}))

;;; Create the neural-map / data structure

;; :x wall, :f end, :s start, :p path, :e empty
(def direction->status {"U" :p, "D" :p, "L" :p, "R" :p, "X" :x, "S" :s, "F" :f})

(defn fill-with [vcoll total-count item]
  (let [diff (- total-count (count vcoll))]
    (if (pos? diff) (apply conj vcoll (take diff (repeat item)))
                    vcoll)))

(defn set-or-create [neuro-map x y type]
  (let [old-value (get-in neuro-map [y x])]
    (if old-value (assoc-in neuro-map [y x] type)
                  (as-> (fill-with (nth neuro-map y []) x :e) $
                        (assoc $ x type)
                        (assoc (fill-with neuro-map y []) y $)))))

(defn direction->coordinate [[x y] direction]
  (case direction "U" [x (dec y)]
                  "D" [x (inc y)]
                  "L" [(dec x) y]
                  "R" [(inc x) y]
                  [x y]))

(defn process-step [neuro-map step-map]
  (loop [nmap neuro-map
         [x y] (first (keys step-map))
         steps (first (vals step-map))]
    (if (seq steps)
      (recur (set-or-create nmap x y (get direction->status (first steps)))
             (direction->coordinate [x y] (first steps))
             (rest steps))
      nmap)))

(def neural-map (reduce (fn [coll item] (process-step coll item)) [] data))

;;; Find the path

(defn find-2d-coords [target nmap]
  (for [[y row] (map-indexed vector nmap)
        [x item] (map-indexed vector row)
        :when (= item target)]
    [x y]))

(defn valid-sym? [sym] (#{:p :f} sym))

(defn get-neighbors [nmap x y]
  (merge (when (valid-sym? (get-in nmap [y (dec x)] nil)) {:left [(dec x) y "L"]})
         (when (valid-sym? (get-in nmap [y (inc x)] nil)) {:right [(inc x) y "R"]})
         (when (valid-sym? (get-in nmap [(dec y) x] nil)) {:up [x (dec y) "U"]})
         (when (valid-sym? (get-in nmap [(inc y) x] nil)) {:down [x (inc y) "D"]})))

(defn breadth-search [nmap x y]
  (let [queue (atom [])
        visited (atom {})]
    (swap! visited assoc [y x] {:ancestor nil :direction nil})
    (swap! queue conj [y x])
    (loop [[y_ x_] (peek @queue)]
      (reset! queue (vec (remove #{[y_ x_]} @queue)))
      (if (= :f (get-in nmap [y_ x_]))
        [y_ x_ @visited]
        (do (doseq [neighbor (vals (get-neighbors nmap x_ y_))
                    :let [[x__ y__ direction] neighbor]
                    :when (empty? (filter #(= [y__ x__] %) (keys @visited)))]
              (swap! visited assoc [y__ x__] {:ancestor [y_ x_] :direction direction})
              (swap! queue conj [y__ x__]))
            (if (peek @queue)
              (recur (peek @queue))
              "Ran out of queue!"))))))

;;; Build the answer format

(defn build-path [y x path-map]
  (loop [string ""
         node (get path-map [y x])]
    (let [{:keys [direction ancestor]} node]
      (if ancestor
        (recur (str direction string)
               (get path-map ancestor))
        string))))

(def test-data [[:s :e :e :p]
                [:p :p :e :p]
                [:p :e :p :p]
                [:p :p :p :f]])

(comment
  data
  neural-map
  (find-2d-coords :s neural-map)

  (let [[y x path-map] (breadth-search test-data 0 0)]
    (build-path y x path-map))

  ;; Puzzle solution
  (let [[y x path-map] (breadth-search neural-map 2 2)]
    (build-path y x path-map)))
