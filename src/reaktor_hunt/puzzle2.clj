(ns reaktor-hunt.puzzle2)

(def signal (slurp "/home/matias/Work/reaktor-hunt/resources/puzzle2"))

(defn find-most-common [data]
  (->> (frequencies data)
       (sort-by second >)
       ffirst))

(defn find-next-chars [c]
  (->> (partition 2 1 signal)
      (eduction (filter #(= (first %) c))
                (map second))
      (apply str)))

(comment
  (loop [search-string signal
         next-char (find-most-common search-string)
         password ""]
    (if (= \; next-char)
      password
      (let [next-string (find-next-chars next-char)]
        (recur next-string
               (find-most-common next-string)
               (str password next-char))))))
